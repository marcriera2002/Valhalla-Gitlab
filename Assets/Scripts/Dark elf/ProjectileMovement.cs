﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProjectileMovement : MonoBehaviour
{
    public float ProjectileSpeed;
    public bool moving = true;
    private  FixedJoint follow;
    public float arrowTimeScreen = 8;
    public float damage;

    Rigidbody m_Rigidbody;
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        transform.LookAt(GameObject.Find("PlayerTarget").transform);
        
       
    }
    void Update()
    {
        if (moving==true){
            m_Rigidbody.velocity = transform.forward*ProjectileSpeed*1000*Time.deltaTime;
        }
        transform.position = new Vector3(transform.position.x,transform.position.y,0);
    }
    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player" || other.gameObject.layer  == LayerMask.NameToLayer("Obstacle")){
            Debug.Log("colision   "+other.name+ "  "+ gameObject.name);
            moving=false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            m_Rigidbody.velocity=new Vector3(0,0,0);
            m_Rigidbody.useGravity=false;
            gameObject.transform.parent = other.gameObject.transform;
            StartCoroutine(timeOnScreen(arrowTimeScreen));
            if(other.tag=="Player"){
                other.gameObject.GetComponent<PlayerHitReceive>().HitReceive(damage);
            }
    }
    }
    IEnumerator timeOnScreen(float time){
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
    
}
