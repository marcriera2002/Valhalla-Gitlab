﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitReceiveAndDeath : MonoBehaviour
{
    public bool SingleHit = true;
    public Enemy Stats;
    public float Hp;
    public GameObject Weapon;
    public bool attacking;
    private bool death = false;
    private bool onEffect = false;
    private enemy_movement movementController;
    private EffectsController enemyEffects;
    private int effectLevel;

    public bool gotHit;
    public bool dealtDamage;
    public bool boss = false;
    private void Start() {
        enemyEffects = gameObject.GetComponentInChildren<EffectsController>();
        Hp = Stats.Hp;
        ani = gameObject.GetComponent<Animator>();
        movementController = gameObject.GetComponent<enemy_movement>();
    }
    private Animator ani;
    private void Update() {
        if (Hp <=0 && !death){
            death = true;
            Death();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (GameObject.FindGameObjectWithTag("Player")!=null&&GameObject.FindGameObjectWithTag("Player").GetComponent<AttackController>().attacking 
            && other.CompareTag("Weapon") 
            && SingleHit 
            && !death){
            AudioManager.instance.Play("Hit");
            SingleHit = false;
            Debug.Log("hit: "+ other.GetComponentInParent<WeaponController>().GetDamage());
            Hp -= other.GetComponentInParent<WeaponController>().GetDamage();
            if (PlayerManager.Instance.ActiveWeaponIndex == 0)
            {
                applyEffects();
            }
            gotHit = true;
            StartCoroutine(gotHitReset());
        }

        if(other.CompareTag("Player") && attacking){
            AudioManager.instance.Play("Hit");
            Debug.Log("EnemyHitWeapon");
            other.gameObject.GetComponent<PlayerHitReceive>().HitReceive(Stats.WeaponDamage);
            dealtDamage = true;
            StartCoroutine(dealtDamageReset());
        }
    }

    private void OnCollisionEnter(Collision other) {
        if(other.gameObject.CompareTag("Player")){
            Debug.Log("EnemyHitBody");
            other.gameObject.GetComponent<PlayerHitReceive>().HitReceive(Stats.BodyDamage);
            dealtDamage = true;
            StartCoroutine(dealtDamageReset());
        }
    }
    public void ResetHit()
    {
        SingleHit = true;
    }

    public void Death(){
        gameObject.GetComponent<enemy_movement>().enabled = false;
        gameObject.GetComponent<FieldOfView>().enabled = false;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        PlayerManager.Instance.PlayerGainMana(5);
        ani.SetTrigger("Death");
    }

    private void applyEffects()
    {
        if (!onEffect)
        {
            switch (getRune(0).Name)
            {
                case "Fire":
                    fireEffect();
                    break;
                case "Potion":
                    potionEffect();
                    break;
                case "Ice":
                    iceEffect();
                    break;
                case "Thunder":
                    thunderEffect();
                    break;
            }
        }
        //Fire effect
        //PlayerManager.Instance.WeaponController;
    }

    private Runes getRune(int position) {
        return PlayerManager.Instance.ActiveInventory.Runes[0].Runes[position];
    }

    private void fireEffect()
    {
        effectLevel = 1;
        onEffect = true;        
        float damage = getRune(0).StatUp;
        if (getRune(0).Equals(getRune(2)))
        {
            damage *= 3;
            effectLevel = 3;
        }
        else if (getRune(0).Equals(getRune(1)))
        {
            damage *= 2;
            effectLevel = 2;
        }
        StartCoroutine(fireOnEffect(damage));
    }
    private void potionEffect()
    {
        effectLevel = 1;
        onEffect = true;        
        float damage = getRune(0).StatUp;
        if (getRune(0).Equals(getRune(2)))
        {
            damage *= 3;
            effectLevel = 3;
        }
        else if (getRune(0).Equals(getRune(1)))
        {
            damage *= 2;
            effectLevel = 2;
        }
        StartCoroutine(potionOnEffect(damage));
    }

    private void iceEffect()
    {
        if (boss == false)
        {
            effectLevel = 1;
            onEffect = true;
            float time = getRune(0).StatUp;
            if (getRune(0).Equals(getRune(2)))
            {
                time *= 3;
                effectLevel = 3;
            }
            else if (getRune(0).Equals(getRune(1)))
            {
                time *= 2;
                effectLevel = 2;
            }
            StartCoroutine(iceOnEffect(time));
        }
    }
    private void thunderEffect()
    {
        effectLevel = 1;
        onEffect = true;        
        float damage = getRune(0).StatUp;
        if (getRune(0).Equals(getRune(2)))
        {
            damage *= 3;
            effectLevel = 3;
        }
        else if (getRune(0).Equals(getRune(1)))
        {
            damage *= 2;
            effectLevel = 2;
        }
        StartCoroutine(thunderOnEffect(damage));
    }


    IEnumerator fireOnEffect(float damage)
    {
        enemyEffects.ActivateEffect(0,effectLevel);        
        for (int i=0; i < 8; i++)
        {
            yield return new WaitForSeconds(0.5f);
            Hp -= damage;
            damage *= 0.8f;
        }
        onEffect = false;
        enemyEffects.DesactiveEffects();
    }
    IEnumerator potionOnEffect(float damage)
    {
        enemyEffects.ActivateEffect(1, effectLevel);        
        for (int i = 0; i < 6; i++)
        {
            yield return new WaitForSeconds(1f);
            Hp -= damage;
        }
        onEffect = false;
        enemyEffects.DesactiveEffects();
    }

    IEnumerator iceOnEffect(float time)
    {
        enemyEffects.ActivateEffect(2, effectLevel);        
        movementController.onIce = true;
        float tmpSpeed = ani.speed;
        ani.speed=0;
        Debug.Log(time);
        yield return new WaitForSeconds(time);
        Debug.Log("end ice");
        movementController.onIce = false;
        ani.speed = tmpSpeed;
        onEffect = false;
        enemyEffects.DesactiveEffects();
    }

    IEnumerator thunderOnEffect(float damage)
    {
        enemyEffects.ActivateEffect(3, effectLevel);        
        yield return new WaitForSeconds(4f);
        Hp -= damage;
        onEffect = false;
        enemyEffects.DesactiveEffects();
    }

    IEnumerator gotHitReset()
    {
        yield return new WaitForSeconds(6);
        gotHit = false;
    }
    IEnumerator dealtDamageReset()
    {
        yield return new WaitForSeconds(6);
        dealtDamage = false;
    }
}
