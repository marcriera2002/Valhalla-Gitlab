﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Orb,
    Rune,
    Coin,
    Weapon,
    Armor
}
public class ItemController : MonoBehaviour
{
    public long Id;
    public Items Item;
    public ItemType Type;

    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        if (LevelManager.Instance.items.Contains(Id)&&Id!=0)
        {
            Destroy(gameObject.transform.parent.gameObject);
        }
        SetIcon();
    }

    public void SetIcon()
    {
        if (Item != null)
        {
            switch (Type)
            {
                case ItemType.Rune:
                case ItemType.Weapon:
                case ItemType.Armor:
                    gameObject.GetComponent<SpriteRenderer>().sprite = Item.Icon;
                    break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            switch (Type)
            {
                case ItemType.Orb:
                    PlayerManager.Instance.Inventory.StatOrbs.Add((StatOrbs)Item);
                    AudioManager.instance.Play("Take Orb");
                    break;
                case ItemType.Rune:
                    PlayerManager.Instance.Inventory.Runes.Add((Runes)Item);
                    PlayerManager.Instance.PauseUIController.FillRunesList();
                    AudioManager.instance.Play("Take Orb");
                    break;
                case ItemType.Coin:
                    PlayerManager.Instance.Coins++;
                    AudioManager.instance.Play("Take Coin");
                    break;
                case ItemType.Weapon:
                    PlayerManager.Instance.Inventory.Weapons.Add((Weapon)Item);
                    AudioManager.instance.Play("Take Orb");
                    break;
                case ItemType.Armor:
                    PlayerManager.Instance.Inventory.Armor.Add((Armor)Item);
                    AudioManager.instance.Play("Take Orb");
                    break;
            }
            LevelManager.Instance.items.Add(Id);
            Destroy(gameObject.transform.parent.gameObject);
        }
    }
}
