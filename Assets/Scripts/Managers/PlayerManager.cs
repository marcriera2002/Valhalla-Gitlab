﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    private static PlayerManager _instance;
    public static PlayerManager Instance { get { return _instance; } }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public Inventory Inventory;
    public ActiveInventory ActiveInventory;
    public int Coins;
    public float Hp;
    [SerializeField]
    private float maxHp;
    public float Mana;
    [SerializeField]
    private float maxMana;
    public WeaponController WeaponController;
    public PauseUIController PauseUIController;
    public BlockController BlockController;
    public int ActiveWeaponIndex = 0;
    public bool DoubleJumpsUnlocked=false;

    private Animator ani;
    private bool playerDead = false;
    public bool MovementAvailable = true;

    // Start is called before the first frame update
    void Start()
    {
        Inventory = Instantiate(Inventory);
        ActiveInventory = Instantiate(ActiveInventory);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InitPlayerAnimator(Animator newPlAni)
    {
        ani = newPlAni;
    }

    public void InitWeaponController(WeaponController newWC)
    {
        WeaponController = newWC;
    }

    public void InitBlockController(BlockController newBC)
    {
        BlockController = newBC;
    }

    public void InitPauseUIController(PauseUIController newPUIC)
    {
        PauseUIController = newPUIC;
    }

    public void PlayerReciveDamage(float damage)
    {
        Hp -= damage;
        if (Hp <= 0)
        {
            PlayerDead();
        }
    }

    public void PlayerUsesMagic(float mana)
    {        
        if (mana >= 0)
        {
            Mana -= mana;
        }
    }

    public void PlayerDead()
    {
        if (playerDead == false){
            playerDead = true;
            MovementAvailable = false;
            ani.SetTrigger("Death");
            LevelManager.Instance.LevelUIController.ShowGameOver();
        }
    }

    public void PlayerGainHp(float gainedHp)
    {
        Hp += gainedHp;
        if (Hp > GetMaxHp())
        {
            Hp = GetMaxHp();
        }
    }

    public void PlayerGainMana(float gainedMana)
    {
        Mana += gainedMana;
        if (Mana > GetMaxMana())
        {
            Mana = GetMaxMana();
        }
    }

    public Weapon GetActiveWeapon()
    {
        return ActiveInventory.Weapons[ActiveWeaponIndex];
    }

    public Weapon ChangeWeapon(bool direction)
    {
        if (direction)
        {
            ActiveWeaponIndex++;
            if (ActiveWeaponIndex > ActiveInventory.Weapons.Length - 1)
            {
                ActiveWeaponIndex = 0;
            }
        }
        else
        {
            ActiveWeaponIndex--;
            if (ActiveWeaponIndex < 0)
            {
                ActiveWeaponIndex = ActiveInventory.Weapons.Length - 1;
            }
        }

        if (ActiveWeaponIndex != 0)
        {
            AudioManager.instance.Stop("Fire/Poison on weapon");
            AudioManager.instance.Stop("IceAura");
            AudioManager.instance.Stop("ThunderAura");
        }       
        return ActiveInventory.Weapons[ActiveWeaponIndex];
        /*LevelManager.Instance.UIController.updateWeapons(Weapons, ActiveWeaponIndex);
        if (LevelManager.Instance.Gun != null)
        {
            LevelManager.Instance.Gun.GetComponent<GunController>().changeWeapon(Weapons[ActiveWeaponIndex]);
        }*/
    }

    public Armor GetActiveArmor()
    {
        return ActiveInventory.ArmorShield;
    }

    public float GetMaxHp()
    {
        float toAdd = 0;
        foreach(Runes rune in ActiveInventory.Runes[2].Runes)
        {
            if (rune.Name.Equals("MoreHP")){
                toAdd += rune.StatUp;
            }
        }
        foreach (StatOrbs statOrb in Inventory.StatOrbs)
        {
            if (statOrb.Name.Equals("MoreHP"))
            {
                toAdd += statOrb.StatsUp;
            }
        }
        return maxHp+toAdd;
    }

    public float GetMaxMana()
    {
        float toAdd = 0;
        foreach (Runes rune in ActiveInventory.Runes[2].Runes)
        {
            if (rune.Name.Equals("MoreMana"))
            {
                toAdd += rune.StatUp;
            }
        }
        foreach (StatOrbs statOrb in Inventory.StatOrbs)
        {
            if (statOrb.Name.Equals("MoreMana"))
            {
                toAdd += statOrb.StatsUp;
            }
        }
        return maxMana + toAdd;
    }

    public void RevivePlayer()
    {
        Hp = GetMaxHp();
        Mana = GetMaxMana();
        MovementAvailable = true;
        playerDead = false;
    }

    public void ReloadTriangleRuneEffects()
    {
        WeaponController.ReloadVisualEffects();        
    }

    public void ReloadCircleRuneEffects()
    {
        if (GetMaxHp() < Hp)
        {
            Hp = GetMaxHp();
        }
        if (GetMaxMana() < Mana)
        {
            Mana = GetMaxMana();
        }
    }
}
