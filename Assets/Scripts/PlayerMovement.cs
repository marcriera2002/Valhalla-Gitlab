﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Vector2 direction;
    [SerializeField]
    private float velocity;
    private Animator ani;
    private bool rotated;

    void Start()
    {
        ani = GetComponent<Animator>();
    }
    
    void Update()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            direction = new Vector2(Input.GetAxis("Horizontal"), 0);
            ani.SetBool("run", true);
        }
        else
        {
            ani.SetBool("run", false);
        }
        Movement();
    }

    public void Movement()
    {        
        transform.position = new Vector3(transform.position.x + (velocity * Time.deltaTime) * direction.x, transform.position.y, 0);
        if (Input.GetAxis("Horizontal") == 0){}
        else if (Input.GetAxis("Horizontal") < 0)
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, -90, transform.localEulerAngles.z);
            rotated = true;
        }
        else
        {
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, 90, transform.localEulerAngles.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            //LevelManager.Instance.Coins++;
            Destroy(other.gameObject.transform.parent.gameObject);
        }
    }
}
