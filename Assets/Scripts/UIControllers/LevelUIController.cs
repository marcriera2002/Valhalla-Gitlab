﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUIController : MonoBehaviour
{
    public GameObject PauseMenuUI;
    public GameObject GameOverPanel;
    public GameObject WinPanel;    
    public Slider SliderHP;
    public Slider SliderBossHP;
    public Slider SliderMana;
    public Image[] Weapons;
    public Text CoinsText;
    private BossBehaviour boss=null;

    private void Awake()
    {
        LevelManager.Instance.InitLevelUIController(this);
    }
    // Start is called before the first frame update
    void Start()
    {       
    }

    // Update is called once per frame
    void Update()
    {
        CoinsText.text = PlayerManager.Instance.Coins.ToString();
        SliderHP.GetComponent<RectTransform>().sizeDelta=new Vector2(PlayerManager.Instance.GetMaxHp()*4,100);
        SliderHP.maxValue = PlayerManager.Instance.GetMaxHp();
        SliderHP.value = PlayerManager.Instance.Hp;
        if (boss != null) {
            SliderBossHP.maxValue = boss.GetMaxHp();
            SliderBossHP.value = boss.GetHp();
        }
        SliderMana.GetComponent<RectTransform>().sizeDelta = new Vector2(PlayerManager.Instance.GetMaxMana() * 4, 85);
        SliderMana.maxValue = PlayerManager.Instance.GetMaxMana();
        SliderMana.value = PlayerManager.Instance.Mana;        
        if (Input.GetButtonDown("Pause")&&(PlayerManager.Instance.MovementAvailable|| Time.timeScale==0))
        {
            PauseMenuUI.SetActive(!PauseMenuUI.activeSelf);            
            Time.timeScale = 1-Time.timeScale;
            PlayerManager.Instance.MovementAvailable = !PlayerManager.Instance.MovementAvailable;
            if (!PauseMenuUI.activeSelf)
            {
                AudioManager.instance.UnPauseAll();
            }
            else
            {
                AudioManager.instance.PauseAll();
            }
        }
    }

    public void InitBoss(BossBehaviour newBoss)
    {
        boss = newBoss;
    }

    public void ShowGameOver()
    {
        GameOverPanel.SetActive(true);
        SliderBossHP.gameObject.SetActive(false);
        AudioManager.instance.PauseAll();
    }
    public void ExitLevel()
    {
        LevelManager.Instance.ExitLevel();
        AudioManager.instance.StopAll();
        AudioManager.instance.Play("Menu Theme");        
    }
    public void ReloadLevel()
    {
        LevelManager.Instance.ReloadLevel();
        AudioManager.instance.UnPauseAll();
    }

    public void ShowWin()
    {
        PlayerManager.Instance.MovementAvailable = false;
        WinPanel.SetActive(true);
    }

    public void Continue(){
        PlayerManager.Instance.MovementAvailable = true;
        WinPanel.SetActive(false);

    }

    public void ReloadWeapons()
    {
        Weapons[0].sprite = PlayerManager.Instance.ActiveInventory.Weapons[(PlayerManager.Instance.ActiveWeaponIndex - 1+3) % 3].Icon;
        Weapons[1].sprite = PlayerManager.Instance.ActiveInventory.Weapons[PlayerManager.Instance.ActiveWeaponIndex].Icon;
        Weapons[2].sprite = PlayerManager.Instance.ActiveInventory.Weapons[(PlayerManager.Instance.ActiveWeaponIndex + 1) % 3].Icon;
    }
}
