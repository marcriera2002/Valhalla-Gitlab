﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUIController : MonoBehaviour
{
    public GameObject[] Panels;

    public Text EquipmentText;
    public GameObject WeaponContent;
    public GameObject WeaponItem;
    public int WeaponsPerRow = 6;
    public Image[] ActiveWeapons = new Image[3];
    public GameObject ArmorContent;
    public GameObject ArmorItem;
    public int ArmorPerRow = 6;
    public Image[] ActiveArmor = new Image[2];

    public Text RunesText;
    public GameObject RunesContent;
    public GameObject RunesItem;
    public int RunesPerRow = 7;
    public Image[] ActiveRunesWeapons = new Image[3];
    public Image[] ActiveTriangleRunes = new Image[3];
    public Image[] ActiveSquareRunes = new Image[3];
    public Image[] ActiveCircleRunes = new Image[3];
    public Image[] SelectedImageButtonsRuneWeapon = new Image[3];
    public GameObject[] ExtraRunes;

    public Text ControlsText;

    public Text OptionsText;
    public Text SoundEffectsText;
    public Text MusicText;    
    public Text MainMenuText;
    public Text QuitGameText;
    public Text MainMenuGameOverText;
    public Text RetryGameOverText;
    public Text MainMenuWinText;
    public Text ContinueWinText;
    public Dropdown LangSelector;

    public Sprite[] OptionsIcons;
    public Sprite[] SelectedIcons;
    public Sprite[] RuneEquipedIcons;
    public Sprite[] RuneOptionsIcons;
    public Sprite EmptyIcon;

    internal int ActiveRuneWeaponIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        PlayerManager.Instance.InitPauseUIController(this);
        FillWeaponsList();
        FillArmorList();
        FillRunesList();
        reloadTexts();
        ReloadActiveWeaponsIcons();
        ReloadActiveArmorIcons();
        ReloadActiveRunesIcons();
        generateDropdownLangs();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeTab(int position)
    {
        foreach (GameObject panel in Panels)
        {
            panel.SetActive(false);
        }
        Panels[position].SetActive(true);
        AudioManager.instance.Play("Change page");
    }

    private void reloadTexts()
    {
        EquipmentText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Equipment");
        RunesText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Runes");
        OptionsText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Options");
        SoundEffectsText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("SoundEffects");
        MusicText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Music");        
        MainMenuText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("MainMenu");
        QuitGameText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("QuitGame");
        MainMenuGameOverText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("MainMenu");
        RetryGameOverText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Retry");
        MainMenuWinText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("MainMenu");
        ContinueWinText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Continue");
        ControlsText.text = GameManager.Instance.Texts[GameManager.Instance.SelectedLang].GetValue("Controls");
    }

    public void ReloadActiveWeaponsIcons()
    {
        ActiveWeapons[0].sprite = PlayerManager.Instance.ActiveInventory.Weapons[0].Icon;
        ActiveWeapons[1].sprite = PlayerManager.Instance.ActiveInventory.Weapons[1].Icon;
        ActiveWeapons[2].sprite = PlayerManager.Instance.ActiveInventory.Weapons[2].Icon;
        ActiveRunesWeapons[0].sprite = PlayerManager.Instance.ActiveInventory.Weapons[0].Icon;
        ActiveRunesWeapons[1].sprite = PlayerManager.Instance.ActiveInventory.Weapons[1].Icon;
        ActiveRunesWeapons[2].sprite = PlayerManager.Instance.ActiveInventory.Weapons[2].Icon;
    }

    public void ReloadActiveArmorIcons()
    {
        ActiveArmor[0].sprite = PlayerManager.Instance.ActiveInventory.ArmorSet.Icon;
        ActiveArmor[1].sprite = PlayerManager.Instance.ActiveInventory.ArmorShield.Icon;
    }

    public void ReloadActiveRunesIcons()
    {
        foreach (Image selectedImage in SelectedImageButtonsRuneWeapon)
        {
            selectedImage.sprite = SelectedIcons[0];
        }
        SelectedImageButtonsRuneWeapon[ActiveRuneWeaponIndex].sprite = SelectedIcons[1];

        if (ActiveRuneWeaponIndex == 0)
        {
            ActiveTriangleRunes[0].sprite = PlayerManager.Instance.ActiveInventory.Runes[0].Runes[0].Icon;
            ActiveTriangleRunes[1].sprite = PlayerManager.Instance.ActiveInventory.Runes[0].Runes[1].Icon;
            ActiveTriangleRunes[2].sprite = PlayerManager.Instance.ActiveInventory.Runes[0].Runes[2].Icon;
            ActiveSquareRunes[0].sprite = PlayerManager.Instance.ActiveInventory.Runes[1].Runes[0].Icon;
            ActiveSquareRunes[1].sprite = PlayerManager.Instance.ActiveInventory.Runes[1].Runes[1].Icon;
            ActiveSquareRunes[2].sprite = PlayerManager.Instance.ActiveInventory.Runes[1].Runes[2].Icon;
            ActiveCircleRunes[0].sprite = PlayerManager.Instance.ActiveInventory.Runes[2].Runes[0].Icon;
            ActiveCircleRunes[1].sprite = PlayerManager.Instance.ActiveInventory.Runes[2].Runes[1].Icon;
            ActiveCircleRunes[2].sprite = PlayerManager.Instance.ActiveInventory.Runes[2].Runes[2].Icon;
            foreach (GameObject runePanel in ExtraRunes)
            {
                runePanel.SetActive(true);
            }
        }
        else
        {
            ActiveSquareRunes[0].sprite = PlayerManager.Instance.ActiveInventory.Runes[ActiveRuneWeaponIndex + 2].Runes[0].Icon;
            ActiveSquareRunes[1].sprite = PlayerManager.Instance.ActiveInventory.Runes[ActiveRuneWeaponIndex + 2].Runes[1].Icon;
            ActiveSquareRunes[2].sprite = PlayerManager.Instance.ActiveInventory.Runes[ActiveRuneWeaponIndex + 2].Runes[2].Icon;
            foreach (GameObject runePanel in ExtraRunes)
            {
                runePanel.SetActive(false);
            }
        }
    }

    public void FillWeaponsList()
    {
        foreach (Transform child in WeaponContent.transform)
        {
            Destroy(child.gameObject);
        }
        int weaponCounter = 0;
        foreach (Weapon weapon in PlayerManager.Instance.Inventory.Weapons)
        {
            GameObject newItem = Instantiate(WeaponItem);
            newItem.transform.SetParent(WeaponContent.transform);
            newItem.transform.localScale = Vector3.one;
            newItem.GetComponent<Image>().sprite = weapon.Icon;
            newItem.GetComponent<ItemUIController>().Item = weapon;
            weaponCounter++;
            if (weapon.Equals(PlayerManager.Instance.ActiveInventory.Weapons[0]) ||
                weapon.Equals(PlayerManager.Instance.ActiveInventory.Weapons[1]) ||
                weapon.Equals(PlayerManager.Instance.ActiveInventory.Weapons[2]))
            {
                newItem.GetComponent<ItemUIController>().ShowIsActive();
            }
        }
        int rows = (int)Math.Ceiling(weaponCounter / (double)WeaponsPerRow);
        WeaponContent.GetComponent<RectTransform>().sizeDelta = new Vector2(WeaponContent.GetComponent<RectTransform>().sizeDelta.x, 22.5f * 2 + 125 * rows + 25 * (rows - 1));
    }

    public void FillArmorList()
    {
        foreach (Transform child in ArmorContent.transform)
        {
            Destroy(child.gameObject);
        }
        int armorCounter = 0;
        foreach (Armor armor in PlayerManager.Instance.Inventory.Armor)
        {
            GameObject newItem = Instantiate(ArmorItem);
            newItem.transform.SetParent(ArmorContent.transform);
            newItem.transform.localScale = Vector3.one;
            newItem.GetComponent<Image>().sprite = armor.Icon;
            newItem.GetComponent<ItemUIController>().Item = armor;
            armorCounter++;
            if (armor.Equals(PlayerManager.Instance.ActiveInventory.ArmorSet) ||
                armor.Equals(PlayerManager.Instance.ActiveInventory.ArmorShield))
            {
                newItem.GetComponent<ItemUIController>().ShowIsActive();
            }
        }
        int rows = (int)Math.Ceiling(armorCounter / (double)ArmorPerRow);
        ArmorContent.GetComponent<RectTransform>().sizeDelta = new Vector2(ArmorContent.GetComponent<RectTransform>().sizeDelta.x, 22.5f * 2 + 125 * rows + 25 * (rows - 1));
    }

    public void FillRunesList()
    {
        foreach (Transform child in RunesContent.transform)
        {
            Destroy(child.gameObject);
        }
        int runeCounter = 0;
        List<Runes> tmpActiveRunes = new List<Runes>();
        foreach (RuneList runeList in PlayerManager.Instance.ActiveInventory.Runes)
        {
            foreach (Runes rune in runeList.Runes)
            {
                tmpActiveRunes.Add(rune);
            }
        }
        foreach (Runes rune in PlayerManager.Instance.Inventory.Runes)
        {
            GameObject newItem = Instantiate(RunesItem);
            newItem.transform.SetParent(RunesContent.transform);
            newItem.transform.localScale = Vector3.one;
            newItem.GetComponent<Image>().sprite = rune.Icon;
            newItem.GetComponent<ItemUIController>().Item = rune;
            runeCounter++;
            for (int i = 0; i < tmpActiveRunes.Count; i++)
            {
                if (rune.Equals(tmpActiveRunes[i]))
                {
                    newItem.GetComponent<ItemUIController>().ListIndex = i / 3;
                    newItem.GetComponent<ItemUIController>().ItemIndex = i % 3;
                    newItem.GetComponent<ItemUIController>().ShowIsActive();
                    tmpActiveRunes[i] = LevelManager.Instance.EmptyRunes[0];
                    break;
                }
            }
        }
        int rows = (int)Math.Ceiling(runeCounter / (double)RunesPerRow);
        RunesContent.GetComponent<RectTransform>().sizeDelta = new Vector2(RunesContent.GetComponent<RectTransform>().sizeDelta.x, 22.5f * 2 + 125 * rows + 25 * (rows - 1));
    }

    public void ActivateRuneWeapon(int newIndex)
    {
        ActiveRuneWeaponIndex = newIndex;
        ReloadActiveRunesIcons();
    }

    private void generateDropdownLangs()
    {
        LangSelector.options.Clear();
        foreach (LanguageText idiom in GameManager.Instance.Texts)
        {
            LangSelector.options.Add(new Dropdown.OptionData(idiom.Title));
        }
        LangSelector.value = GameManager.Instance.SelectedLang;
        if (GameManager.Instance.SelectedLang == 0)
        {
            LangSelector.value = 1;
            LangSelector.value = 0;
        }
    }

    public void ChangeLang()
    {
        GameManager.Instance.SelectedLang = LangSelector.value;
        reloadTexts();
    }


    public void Exit()
    {
        LevelManager.Instance.Save();
        Application.Quit();
    }
}
